FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/d-c.jar /d-c/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/d-c/app.jar"]
