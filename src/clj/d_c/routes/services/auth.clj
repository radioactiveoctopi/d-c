(ns d-c.routes.services.auth
  (:require [d-c.db.core :as db]
            [ring.util.http-response :as response]
            [buddy.hashers :as hashers]
            [clojure.tools.logging :as log]))


(defn register [{:keys [session]} user]
  (try
    (db/create-user!
     (-> user
         (dissoc :pass-confirm)
         (update :pass hashers/encrypt)))
    (-> {:result :ok}
        (response/ok)
        (assoc :session  (assoc session :identity (:id user))))
    (catch Exception e
      (log/error e)
      (response/internal-server-error
       {:result :error
        :message "Could not add user."}))))



;; (in-ns 'd-c.db.core)
;; (conman/bind-connection *db* "sql/queries.sql")
;; (mount.core/start)
;; (in-ns 'd-c.routes.services.auth)
;; (register {} {:handle "deci" :email "decim@decim1.com" :pass "1234" :pass-confirm "1234"})





















