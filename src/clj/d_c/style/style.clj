(ns d-c.style.style
  (:require [garden.core :refer [css]]
            [garden.stylesheet :as s]
            [garden.color :as color :refer [hsl rgb]]
            [garden.units :refer [px]]
            [garden.def :refer [defstylesheet defstyles]]))

;; Custom URL function for imports
(garden.def/defcssfn url)
            
(defstyles style
  (s/at-import url "https://fonts.googleapis.com/css?family=Open+Sans|Roboto:100,400,700")
  [:body     {:background-color "#010310"
              :color "#fff"
              :font-family "Roboto"}]
  [:div#nav {:width "100%"
             :color "#fff"}])




















