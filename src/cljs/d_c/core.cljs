(ns ^:figwheel-always d-c.core
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [secretary.core :as secretary]
            [goog.events :as events]
            [goog.history.EventType :as HistoryEventType]
            [markdown.core :refer [md->html]]
            [ajax.core :refer [GET POST]]
            [d-c.ajax :refer [load-interceptors!]]
            [d-c.events])
  (:import goog.History))

(defonce app-state (r/atom
                    {:saga
                     {:title "(Diaspora:Cybernet)"
                      :god "Jamal Burgess"}
                     {:top-menu {:1 "Notes" :2 "Books" :3 "Github" :4 "Blurbs" :5 "Me, me, me"}}
                     {:post-list {}}}))
;; Books should have notes as well                       
;; (defn nav-link [uri title page collapsed?]
;;   (let [selected-page (rf/subscribe [:page])]
;;     [:li.nav-item
;;      {:class (when (= page @selected-page) "active")}
;;      [:a.nav-link
;;       {:href uri
;;        :on-click #(reset! collapsed? true)} title]]))
;;(defonce menu (r/atom ["Blog" "Books" "Tags" "Rags"]))

(get-in @app-state [:saga :top-menu][:1])
(defn menu []
  [:span (str "Blog " "Books " "Tags " "Rags ")])


(defn navbar []
  [:div#nav
   [:div
    [:h2  "(Diaspora:Cybernest)"]]
   [:div#menu [menu]]])


(defn post-list []
  ([:div.post-list
    [:h4 "This is a title"]]))
(defn home-page []
  [:div post-list])
          

(def pages
  {:home #'home-page})


(defn page []
  [:div
   [navbar]
   [(pages @(rf/subscribe [:page]))]])

;; -------------------------
;; Routes
(secretary/set-config! :prefix "#")

(secretary/defroute "/" []
  (rf/dispatch [:set-active-page :home]))

;(secretary/defroute "/about" []
 ; (rf/dispatch [:set-active-page :about]))

;; -------------------------
;; History
;; must be called after routes have been defined
(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
      HistoryEventType/NAVIGATE
      (fn [event]
        (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

;; -------------------------
;; Initialize app


(defn mount-components []
  (rf/clear-subscription-cache!)
  (r/render [#'page] (.getElementById js/document "app")))

(defn init! []
  (rf/dispatch-sync [:initialize-db])
  (load-interceptors!)
  (hook-browser-navigation!)
  (mount-components))
