CREATE TABLE users
(id SERIAL PRIMARY KEY,
 handle VARCHAR(30),
 email VARCHAR(30),
 admin BOOLEAN,
 last_login TIMESTAMP,
 is_active BOOLEAN,
 pass VARCHAR(300) NOT NULL);

