CREATE TABLE posts
(id SERIAL PRIMARY KEY,
 title TEXT NOT NULL,
 body TEXT NOT NULL,
 slug TEXT NOT NULL,
 tags TEXT [],
 prev TEXT,
 next Text
);
