(ns d-c.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[d-c started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[d-c has shut down successfully]=-"))
   :middleware identity})
