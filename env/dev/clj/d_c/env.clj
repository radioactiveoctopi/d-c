(ns d-c.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [d-c.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[d-c started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[d-c has shut down successfully]=-"))
   :middleware wrap-dev})
