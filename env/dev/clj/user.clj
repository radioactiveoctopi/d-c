(ns user
  (:require [luminus-migrations.core :as migrations]
            [d-c.config :refer [env]]
            [mount.core :as mount]
            [d-c.figwheel :refer [start-fw stop-fw cljs]]
            d-c.core))

(defn start []
  (mount/start-without #'d-c.core/repl-server))

(defn stop []
  (mount/stop-except #'d-c.core/repl-server))

(defn restart []
  (stop)
  (start))

(defn migrate []
  (migrations/migrate ["migrate"] (select-keys env [:database-url])))

(defn rollback []
  (migrations/migrate ["rollback"] (select-keys env [:database-url])))


