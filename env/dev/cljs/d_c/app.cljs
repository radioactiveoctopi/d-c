(ns ^:figwheel-no-load d-c.app
  (:require [d-c.core :as core]
            [devtools.core :as devtools]))

(enable-console-print!)

(devtools/install!)

(core/init!)
